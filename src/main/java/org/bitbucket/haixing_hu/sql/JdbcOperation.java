/*
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.bitbucket.haixing_hu.sql;

import java.sql.Connection;

/**
 * An interface of the JDBC operations.
 *
 * @param <R>
 *          the type of returned value. If the operation has no return value,
 *          set this type parameter to {@link Void} and returns
 *          {@code null} in the {@link #perform(Connection)} function.
 * @author Haixing Hu
 */
public interface JdbcOperation<R> {

  /**
   * Performs a JDBC operation on a given connection.
   *
   * @param conn
   *          a given JDBC connection.
   * @return the result of operation.
   * @throws Exception
   *           if any error occurred.
   */
  public R perform(Connection conn) throws Exception;
}
