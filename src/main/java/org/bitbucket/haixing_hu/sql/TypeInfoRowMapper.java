/*
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.bitbucket.haixing_hu.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bitbucket.haixing_hu.lang.ArrayUtils;
import org.bitbucket.haixing_hu.sql.error.UnexpectedColumnValueException;

import static org.bitbucket.haixing_hu.sql.TypeInfo.DATA_TYPE;
import static org.bitbucket.haixing_hu.sql.TypeInfo.DATA_TYPE_ENUM;
import static org.bitbucket.haixing_hu.sql.TypeInfo.NULLABLE;
import static org.bitbucket.haixing_hu.sql.TypeInfo.NULLABLE_ENUM;
import static org.bitbucket.haixing_hu.sql.TypeInfo.NUM_PREC_RADIX;
import static org.bitbucket.haixing_hu.sql.TypeInfo.PRECISION;
import static org.bitbucket.haixing_hu.sql.TypeInfo.SEARCHABLE;
import static org.bitbucket.haixing_hu.sql.TypeInfo.SEARCHABLE_ENUM;
import static org.bitbucket.haixing_hu.sql.TypeInfo.TYPE_NAME;

/**
 * A {@link RowMapper} which maps a row of a {@link ResultSet} to a
 * {@link TypeInfo} object.
 *
 * @author Haixing Hu
 */
public final class TypeInfoRowMapper implements RowMapper<TypeInfo> {

  public static final TypeInfoRowMapper INSTANCE = new TypeInfoRowMapper();

  @Override
  public TypeInfo mapRow(final ResultSet rs, final int rowNum) throws SQLException {
    final TypeInfo info = new TypeInfo();
    info.setTypeName(rs.getString(1));
    info.setDataType(rs.getInt(2));
    info.setPrecision(rs.getInt(3));
    info.setLiteralPrefix(rs.getString(4));
    info.setLiteralSuffix(rs.getString(5));
    info.setCreateParams(rs.getString(6));
    info.setNullable(rs.getShort(7));
    info.setCaseSensitive(rs.getBoolean(8));
    info.setSearchable(rs.getShort(9));
    info.setUnsignedAttribute(rs.getBoolean(10));
    info.setFixedPrecScale(rs.getBoolean(11));
    info.setAutoIncrement(rs.getBoolean(12));
    info.setLocalTypeName(rs.getString(13));
    info.setMinimumScale(rs.getShort(14));
    info.setMaximumScale(rs.getShort(15));
    info.setSqlDataType(rs.getInt(16));
    info.setSqlDatetimeSub(rs.getInt(17));
    info.setNumPrecRadix(rs.getInt(18));
    if (info.getTypeName() == null) {
      throw new UnexpectedColumnValueException(TYPE_NAME, null);
    }
    if (ArrayUtils.indexOf(DATA_TYPE_ENUM, info.getDataType()) < 0) {
      throw new UnexpectedColumnValueException(DATA_TYPE, info.getDataType());
    }
    if (info.getPrecision() < 0) {
      throw new UnexpectedColumnValueException(PRECISION, info.getPrecision());
    }
    if (ArrayUtils.indexOf(NULLABLE_ENUM, info.getNullable()) < 0) {
      throw new UnexpectedColumnValueException(NULLABLE, info.getNullable());
    }
    if (ArrayUtils.indexOf(SEARCHABLE_ENUM, info.getSearchable()) < 0) {
      throw new UnexpectedColumnValueException(SEARCHABLE, info.getSearchable());
    }
    if (info.getNumPrecRadix() < 2) {
      throw new UnexpectedColumnValueException(NUM_PREC_RADIX, info.getNumPrecRadix());
    }
    return info;
  }

}
