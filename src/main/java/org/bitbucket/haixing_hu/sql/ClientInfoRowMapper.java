/*
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.bitbucket.haixing_hu.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bitbucket.haixing_hu.sql.error.UnexpectedColumnValueException;

/**
 * A {@link RowMapper} which maps a row of a {@link ResultSet} to a
 * {@link ClientInfo} object.
 *
 * @author Haixing Hu
 */
public final class ClientInfoRowMapper implements RowMapper<ClientInfo> {

  public static final ClientInfoRowMapper INSTANCE = new ClientInfoRowMapper();

  @Override
  public ClientInfo mapRow(final ResultSet rs, final int rowNum)
      throws SQLException {
    final String name = rs.getString(1);
    if (name == null) {
      throw new UnexpectedColumnValueException(1, null);
    }
    final int maxLength = rs.getInt(2);
    if (maxLength < 0) {
      throw new UnexpectedColumnValueException(1, maxLength);
    }
    final String defaultValue = rs.getString(3);
    final String description = rs.getString(4);
    return new ClientInfo(name, maxLength, defaultValue, description);
  }

}
