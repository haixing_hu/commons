/*
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.bitbucket.haixing_hu.sql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * This interface sets values on a {@link PreparedStatement}, for each of a
 * number of updates in a batch using the same SQL.
 * <p>
 * Implementations are responsible for setting any necessary parameters. SQL
 * with placeholders will already have been supplied. Implementations do not
 * need to concern themselves with {@link SQLException} that may be thrown from
 * operations they attempt.
 *
 * @author Haixing Hu
 */
public interface PreparedStatementSetter {

  /**
   * Set parameter values on the given {@link PreparedStatement}.
   *
   * @param ps
   *          the {@link PreparedStatement} to invoke setter methods on.
   * @throws SQLException
   *           if a SQLException is encountered (i.e. there is no need to catch
   *           SQLException)
   */
  public void setValues(PreparedStatement ps) throws SQLException;

}
