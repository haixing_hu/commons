/*
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.bitbucket.haixing_hu.text.xml;

/**
 * Thrown when an XML node has invalid content.
 *
 * @author Haixing Hu
 */
public class InvalidXmlNodeContentException extends XmlException {

  private static final long serialVersionUID = 2084890707891169214L;

  private final String tagName;
  private final String content;

  public InvalidXmlNodeContentException(final String tagName,
      final String content) {
    super(formatMessage(tagName, content));
    this.tagName = tagName;
    this.content = content;
  }

  public InvalidXmlNodeContentException(final String tagName,
      final String content, final Throwable cause) {
    super(formatMessage(tagName, content), cause);
    this.tagName = tagName;
    this.content = content;
  }

  public InvalidXmlNodeContentException(final String tagName,
      final String content, final String message) {
    super(formatMessage(tagName, content) + message);
    this.tagName = tagName;
    this.content = content;
  }

  public InvalidXmlNodeContentException(final String tagName,
      final String content, final String message, final Throwable cause) {
    super(formatMessage(tagName, content) + message, cause);
    this.tagName = tagName;
    this.content = content;
  }

  public String getTagName() {
    return tagName;
  }

  public String getContent() {
    return content;
  }

  public static String formatMessage(final String tagName, final String content) {
    return "Invalid content of node <" + tagName + ">: \"" + content + "\". ";
  }
}
