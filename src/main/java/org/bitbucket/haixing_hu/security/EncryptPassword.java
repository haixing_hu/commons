/*
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.bitbucket.haixing_hu.security;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * This is a simple program used to encrypt the password.
 *
 * @author Haixing Hu
 */
public class EncryptPassword {

  public static void main(final String[] args) {
    for (final String arg : args) {
      try {
        final String encrypted = PasswordEncryptor.encryptPassword(arg);
        System.out.println(encrypted);
      } catch (final NoSuchAlgorithmException e) {
        System.err.println(e.getMessage());
        System.exit(-1);
      } catch (final InvalidKeySpecException e) {
        System.err.println(e.getMessage());
        System.exit(-1);
      }
    }
  }
}
