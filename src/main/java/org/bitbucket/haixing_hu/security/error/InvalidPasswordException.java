/*
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.bitbucket.haixing_hu.security.error;

/**
 * An exception thrown to indicate the password is invalid.
 *
 * @author Haixing Hu
 */
public class InvalidPasswordException extends AuthenticationException {

  private static final long serialVersionUID = 5595569018510974092L;

  private final String username;
  private final String password;

  public InvalidPasswordException() {
    super("The password is invalid.");
    username = null;
    password = null;
  }

  public InvalidPasswordException(final String username, final String password) {
    super("The password for user '" + username + "' is invalid: '" + password + "'");
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }
}
