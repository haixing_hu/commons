/*
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.bitbucket.haixing_hu.util;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import org.bitbucket.haixing_hu.security.PasswordEncryptor;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for the {@link PasswordEncryptor} class.
 *
 * @author Haixing Hu
 */
public class PasswordEncryptorTest {

  /**
   * Test method for {@link PasswordEncryptor#checkPassword(String, byte[], byte[])}.
   */
  @Test
  public void testCheckPassword() throws NoSuchAlgorithmException,
      InvalidKeySpecException {
    final byte[] salt1 = PasswordEncryptor.generateSalt();
    final String password1 = "abcdefg";
    final byte[] encrypted1 = PasswordEncryptor.encryptPassword(password1, salt1);

    assertTrue(PasswordEncryptor.checkPassword("abcdefg", encrypted1, salt1));
    assertFalse(PasswordEncryptor.checkPassword("abcdefh", encrypted1, salt1));
  }

  /**
   * Test method for {@link PasswordEncryptor#checkPassword(String, String)}.
   */
  @Test
  public void testCheckPasswordStringString() throws NoSuchAlgorithmException,
      InvalidKeySpecException {
    final String password1 = "abcdefg";
    final String encrypted1 = PasswordEncryptor.encryptPassword(password1);
    System.out.println("Encrypted password with salt is: " + encrypted1);
    assertTrue(PasswordEncryptor.checkPassword("abcdefg", encrypted1));
    assertFalse(PasswordEncryptor.checkPassword("abcdefh", encrypted1));
  }

  /**
   * Test method for {@link PasswordEncryptor#encryptPassword(String, byte[])}.
   * @throws NoSuchAlgorithmException
   * @throws InvalidKeySpecException
   */
  @Test
  public void testEncryptPassword() throws NoSuchAlgorithmException,
      InvalidKeySpecException {
    final byte[] salt1 = PasswordEncryptor.generateSalt();
    final String password = "abcdefg";

    final byte[] encrypted1 = PasswordEncryptor.encryptPassword(password, salt1);
    assertNotNull(encrypted1);
    System.out.printf("encrypted password length = %d\n", encrypted1.length);
    final byte[] salt2 = PasswordEncryptor.generateSalt();

    final byte[] encrypted2 = PasswordEncryptor.encryptPassword(password, salt2);
    assertNotNull(encrypted2);
    System.out.printf("encrypted password length = %d\n", encrypted2.length);
    assertFalse(Arrays.equals(encrypted1, encrypted2));

    final byte[] salt32 = PasswordEncryptor.generateSalt();
    final String password32 = "12345678901234567890123456789012";
    final byte[] encrypted32 = PasswordEncryptor.encryptPassword(password32, salt32);
    assertNotNull(encrypted32);
    System.out.printf("encrypted password length = %d\n", encrypted32.length);
  }

  /**
   * Test method for {@link PasswordEncryptor#encryptPassword(String)}.
   * @throws NoSuchAlgorithmException
   * @throws InvalidKeySpecException
   */
  @Test
  public void testEncryptPasswordString() throws NoSuchAlgorithmException,
      InvalidKeySpecException {
    final String password = "abcdefg";

    final String encrypted1 = PasswordEncryptor.encryptPassword(password);
    assertNotNull(encrypted1);
    assertEquals(PasswordEncryptor.ENCRYPTED_SALTED_PASSWORD_LENGTH, encrypted1.length());
    System.out.printf("encrypted password = %s\n", encrypted1);

    final String password32 = "12345678901234567890123456789012";
    final String encrypted32 = PasswordEncryptor.encryptPassword(password32);
    assertNotNull(encrypted32);
    assertEquals(PasswordEncryptor.ENCRYPTED_SALTED_PASSWORD_LENGTH, encrypted32.length());
    System.out.printf("encrypted password = %s\n", encrypted32);
  }

  /**
   * Test method for {@link PasswordEncryptor#generateSalt()}.
   */
  @Test
  public void testGenerateSalt() throws NoSuchAlgorithmException {
    final byte[] salt1 = PasswordEncryptor.generateSalt();
    assertNotNull(salt1);
    assertEquals(PasswordEncryptor.SALT_BYTES, salt1.length);

    final byte[] salt2 = PasswordEncryptor.generateSalt();
    assertNotNull(salt2);
    assertEquals(PasswordEncryptor.SALT_BYTES, salt2.length);

    assertFalse(Arrays.equals(salt1, salt2));
  }

}
